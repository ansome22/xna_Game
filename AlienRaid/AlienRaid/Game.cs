using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Xml.Linq;
using System.Diagnostics;

namespace Assignment1 {
	public enum GameState {
		Intro, InPlay, GameOver
	}

	public class ForestGame : Game {
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		Dictionary<string, EnemyType> _alienTypes;
       // Dictionary<int, HighScore> _Highscore;
        LevelData _levelData;
        Texture2D _grass;
        Texture2D _tree;
        Song bgMusic;



        public readonly EnemysComponent AliensComponent;
		public readonly PlayerComponent PlayerComponent;
		public readonly IntroComponent IntroComponent;
		public readonly GameOverComponent GameOverComponent;

		public int Level =1;
        private string grasstexture = "Images/Game/Grass/Grass1";
        private Rectangle _treerec;

        public static ForestGame Current { get; private set; }

		public ForestGame() {
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			Current = this;

			graphics.PreferredBackBufferHeight = 900;
			graphics.PreferredBackBufferWidth = 1200;
			graphics.IsFullScreen = false;
			Window.Title = "Assignment 1";

            //Components.Add(new Trees(this));
            Components.Add(PlayerComponent = new PlayerComponent(this));
			Components.Add(AliensComponent = new EnemysComponent(this));
			Components.Add(IntroComponent = new IntroComponent(this));
			Components.Add(GameOverComponent = new GameOverComponent(this));

			GotoState(GameState.Intro);
		}

		public void StartNewGame(int level) {
			PlayerComponent.InitPlayer();
			AliensComponent.InitAliens();
			InitLevel(level);
		}
        //intilize content
        //eg varibles
		protected override void Initialize() {
			base.Initialize();

			_alienTypes = (from at in XElement.Load("Data/EnemyTypes.xml").Descendants("AlienType")
								select new EnemyType {
									Name = (string)at.Attribute("Name"),
									Score = (int)at.Attribute("Score"),
                                    Hitpoint = (int)at.Attribute("Hitpoint"),
                                    MaxXSpeed = (float)at.Attribute("MaxXSpeed"),
									MaxYSpeed = (float)at.Attribute("MaxYSpeed"),
									Texture = Content.Load<Texture2D>((string)at.Attribute("Texture")),
									FirstFrame = ParseRectangle((string)at.Attribute("FirstFrame")),
									Space = (int)at.Attribute("Space"),
									Frames = (int)at.Attribute("Frames"),
									AnimationRate = TimeSpan.FromMilliseconds((int)at.Attribute("AnimationRate"))
								}).ToDictionary(t => t.Name);


                   //_Highscore = (from at in XElement.Load("Data/HighScore.xml").Descendants("Highscore")
                   //              select new HighScore
                   //              {
                   //                  Score = (int)at.Attribute("Score"),
                   //                  Level = (int)at.Attribute("Level")

                   //              }).ToDictionary(t => t.Level);
            InitLevel(Level);
		}

		public GameState State { get; private set; }

		public void GotoState(GameState state) {
			switch(state) {
				case GameState.Intro:
                    //dont need to see certain screens
					IntroComponent.Enabled = IntroComponent.Visible = true;
					AliensComponent.Enabled = AliensComponent.Visible = false;
					PlayerComponent.Enabled = PlayerComponent.Visible = false;
					GameOverComponent.Enabled = GameOverComponent.Visible = false;

                    //have some music
                    MediaPlayer.Stop();
                    bgMusic = Content.Load<Song>("Sound/Music/Dynamic_Horizon");
                    MediaPlayer.Volume = 0.01f;
                    MediaPlayer.Play(bgMusic);



                    break;
                //dont need to see certain screens
                case GameState.InPlay:
					IntroComponent.Enabled = IntroComponent.Visible = false;
					AliensComponent.Enabled = AliensComponent.Visible = true;
					PlayerComponent.Enabled = PlayerComponent.Visible = true;
					GameOverComponent.Enabled = GameOverComponent.Visible = false;
                    //have some music
                    MediaPlayer.Stop();
                    bgMusic = Content.Load<Song>("Sound/Music/Dynamic_School_Days");
                    MediaPlayer.Volume = 0.01f;
                    MediaPlayer.Play(bgMusic);

                    break;
                //dont need to see certain screens
                case GameState.GameOver:
					IntroComponent.Enabled = IntroComponent.Visible = false;
					AliensComponent.Enabled = AliensComponent.Visible = false;
					PlayerComponent.Enabled = PlayerComponent.Visible = true;
					GameOverComponent.Enabled = GameOverComponent.Visible = true;
                    //have some music
                    MediaPlayer.Stop();
                    bgMusic = Content.Load<Song>("Sound/Music/Dynamic_Daydreams");
                    MediaPlayer.Volume = 0.01f;
                    MediaPlayer.Play(bgMusic);

                    break;
			}
			State = state;
		}

		internal EnemyType GetAlienType(string name) {
			return _alienTypes[name];
		}

		public static Rectangle ParseRectangle(string p) {
			string[] nums = p.Split(',');
			Debug.Assert(nums.Length == 4);
			return new Rectangle(int.Parse(nums[0]), int.Parse(nums[1]), int.Parse(nums[2]), int.Parse(nums[3]));
		}

		protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _tree = Content.Load<Texture2D>("Images/Game/treetopdown");
            _grass = Content.Load<Texture2D>(grasstexture);
            }

		protected override void UnloadContent() {
		}

		protected override void Update(GameTime gameTime) {
			// Allows the player to exit thge game for gamepad
			if(GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
				this.Exit();

            // Allows the player to exit thge game for keyboard
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape)) {
                Exit();
            }

            //to stop grass contrantly being update in intro screen
            if (State == GameState.InPlay)
            {
                grasstexture = "Images/Game/Grass/Grass" + ForestGame.Current.Level;
            }


            //because i decided to have diffrent textures for ewach levels
            //i need to check each level
            //and tell game where each new texture is
            if (ForestGame.Current.Level == 1)
            {
                _treerec = new Rectangle(4, 5, 60, GraphicsDevice.Viewport.Height);
            }
            else if (ForestGame.Current.Level == 2)
            {
                _treerec = new Rectangle(131, 5, 60, GraphicsDevice.Viewport.Height);
            }
            else if (ForestGame.Current.Level == 3)
            {
                _treerec = new Rectangle(67, 5, 60, GraphicsDevice.Viewport.Height);
            }
            else if (ForestGame.Current.Level == 4)
            {
                _treerec = new Rectangle(195, 5, 60, GraphicsDevice.Viewport.Height);
            }




            base.Update(gameTime);
		}

        protected override void Draw(GameTime gameTime) {
            //draw background
            GraphicsDevice.Clear(Color.Green);

            int screenheight = GraphicsDevice.Viewport.Height;
            int screenwidth = GraphicsDevice.Viewport.Width;

            //if player is wanting to play a game
            if (State == GameState.InPlay)
            {
                //Grass
                _grass = Content.Load<Texture2D>(grasstexture);
                spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.AnisotropicWrap, null, null);
                spriteBatch.Draw(_grass,Vector2.Zero, Color.White);
                spriteBatch.End();

                //tree left side
                spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
                spriteBatch.Draw(_tree, Vector2.Zero, _treerec, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0);
                spriteBatch.End();

                //tree right side
                spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
                spriteBatch.Draw(_tree, new Vector2(GraphicsDevice.Viewport.Width - 60, 0), _treerec, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0);
                spriteBatch.End();

            }

            base.Draw(gameTime);
		}

        //an xml containing level data
        //so we need to grab it
		public void InitLevel(int levelNum) {
            _levelData = (from level in XElement.Load("Data/Levels.xml").Descendants("Level")
							  where (int)level.Attribute("Number") == levelNum
							  select new LevelData {
								  Number = levelNum,
								  ChangeDirChance = (int)level.Attribute("ChangeDirChance"),
								  MaxActiveAliens = (int)level.Attribute("MaxActiveAliens"),
								  TotalAliensToFinish = (int)level.Attribute("TotalAliensToFinish"),
								  Boss = _alienTypes[(string)level.Attribute("Boss")],
								  FireChance = (int)level.Attribute("FireChance"),
								  MaxAlienBullets = (int)level.Attribute("MaxAlienBullets"),
								  AlienGenerationTime = TimeSpan.FromMilliseconds((int)level.Attribute("AlienGenerationTime")),
								  SelectionData = (from sel in level.Descendants("AlienType")
														 select new AlienSelectionData {
															 Chance = (int)sel.Attribute("Chance"),
															 Alien = _alienTypes[(string)sel.Attribute("Name")]
														 }).ToList()
							  }).SingleOrDefault();
			Debug.Assert(_levelData != null);
		}

		internal LevelData GetCurrentLevelData() {
			return _levelData;
		}


    }
}
