using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Assignment1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class IntroComponent : DrawableGameComponent
    {
        SpriteBatch _batch;
        SpriteFont _font, _smallFont;
        KeyboardState _previousKS;
        GamePadState _previousGP;
        Texture2D _bg;
        Texture2D _tree;
        Rectangle _treerec = new Rectangle(8, 160, 112, 129);
        public static readonly System.Random r = new System.Random();

        public int Highscore = r.Next(500, 9000);

        int _treerecx = 8;
        int _treerecy  = 160;

        public int level { get; private set; }
        public int minlevel { get; private set; }
        public int maxlevel { get; private set; }
        public Vector2 tree { get; private set; }

        public IntroComponent(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            base.Initialize();
            _batch = new SpriteBatch(GraphicsDevice);
            level = 1;
            maxlevel = 4;
            minlevel = 1;


        }

        protected override void LoadContent()
        {

            _font = Game.Content.Load<SpriteFont>("fonts/bigfont");
            _smallFont = Game.Content.Load<SpriteFont>("fonts/Stats");
            _bg = Game.Content.Load<Texture2D>("Images/Game/Menu/forest-bg");
            _tree = Game.Content.Load<Texture2D>("Images/Game/tree");

            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// need previousKS because key checks far to quick for the user
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            var ks = Keyboard.GetState();
            //when user presses enter start game
            if (_previousKS.IsKeyDown(Keys.Enter) && ks.IsKeyUp(Keys.Enter) || GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.A))
            {
                ForestGame.Current.Level = level;
                ForestGame.Current.GotoState(GameState.InPlay);
                ForestGame.Current.StartNewGame(level);
            }

            //when user presses right key intrement level
            //used to select level
            if (_previousKS.IsKeyDown(Keys.Right) && ks.IsKeyUp(Keys.Right) && level < maxlevel)
            {
                level = level + 1;
            }
            //when user presses left key deintrement level
            //used to select level
            if (_previousKS.IsKeyDown(Keys.Left) && ks.IsKeyUp(Keys.Left) && level > minlevel)
            {
                level = level - 1;
            }
            _previousKS = ks;

            _previousGP = GamePad.GetState(PlayerIndex.One);


            if (level == 1)
            {
                _treerecx = 8;
                _treerecy = 160;
                _treerec = new Rectangle(_treerecx, _treerecy, 112, 129);
            }
            else if (level == 2)
            {
                _treerecx = 136;
                _treerecy = 160;
                _treerec = new Rectangle(_treerecx, _treerecy, 112, 129);
            }
            else if (level == 3)
            {
                _treerecx = 264;
                _treerecy = 160;
                _treerec = new Rectangle(_treerecx, _treerecy, 112, 129);
            }
            else if (level == 4)
            {
                _treerecx = 392;
                _treerecy = 160;
                _treerec = new Rectangle(_treerecx, _treerecy, 112, 129);
            }



            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin();
            _batch.Draw(_bg, GraphicsDevice.Viewport.Bounds, Color.White);

            _batch.DrawString(_font, "Forest Shooter", new Vector2(350, 250), Color.Brown);

            _batch.Draw(_tree, new Vector2(400, 300), _treerec, Color.White);

            _batch.DrawString(_font, "Level: " + level, new Vector2(400, 450), Color.Brown);
            _batch.DrawString(_font, "Highscore: "+ Highscore, new Vector2(400, 500), Color.Brown);

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                _batch.DrawString(_smallFont, "Press A to play", new Vector2(400, 600), Color.Brown);
            }else {
            _batch.DrawString(_smallFont, "Press ENTER to play", new Vector2(400, 600), Color.Brown);
            }
            _batch.End();

            base.Draw(gameTime);
        }


    }
}
