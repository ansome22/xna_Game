using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Assignment1
{
    public class PlayerComponent : DrawableGameComponent
    {
        Texture2D _playerIcon;
        Texture2D _playerModel;
        Sprite _player;
        SpriteBatch _batch;

        Texture2D _attackTexture;
        readonly Sprite[] _bullets = new Sprite[30];    // assume no more than 30 active bullets
        TimeSpan _minForBullet = TimeSpan.FromMilliseconds(400);
        TimeSpan _totalForBulletElapsed;
        int _lastBulletIndex;
        Sprite _playerDeath;
        Texture2D _deathTexture;

        SoundEffect _bulletSound;
        readonly SoundEffectInstance[] _bulletSoundInstance = new SoundEffectInstance[30];
        SoundEffect _alienDeathSound;
        SoundEffect _playerPainSound;
        SoundEffect _playerDeathSound;
        readonly SoundEffectInstance[] _explodeSoundInstance = new SoundEffectInstance[30];

        public int Score;
        public int Power = 1;
        public int Lives = 3;
        private SpriteFont _statsFont;
        Rectangle test = new Rectangle(17, 15, 30, 47);
        private State CurrentState = State.Up;

        enum State{
            left,
            Right,
            Up,
            Down
        }

        public PlayerComponent(Game game)
            : base(game)
        {
        }

        public Sprite Player { get { return _player; } }

        public override void Initialize()
        {
            base.Initialize();
            //intiatize player
            _player = new Sprite(_playerModel, test,8,true,34);
            _player.Position = new Vector2(200, 450);
            _player.AnimationInterval = TimeSpan.FromMilliseconds(200);
            _player.Origin = new Vector2(_player.FrameWidth / 2, 0);
            _player.Scale = 1;

            _playerDeath = new Sprite(_deathTexture, new Rectangle(0, 14, 63, 49), 2, true, 65);
            _playerDeath.Active = false;
            _playerDeath.Origin.X = _playerDeath.FrameWidth / 2;
            _playerDeath.AnimationInterval = TimeSpan.FromMilliseconds(1200);
            _playerDeath.OneShotAnimation = true;
            _playerDeath.Scale = 1;

            for (int i = 0; i < _explodeSoundInstance.Length; i++)
                _explodeSoundInstance[i] = _alienDeathSound.CreateInstance();

            // prepare bullets
            for (int i = 0; i < _bullets.Length; i++)
            {
                Sprite bullet = new Sprite(_attackTexture);
                bullet.Active = false;
                bullet.Scale = 2;
                bullet.ZLayer = .7f;
                bullet.Origin = new Vector2(bullet.FrameWidth / 2, 0);
                _bullets[i] = bullet;

                _bulletSoundInstance[i] = _bulletSound.CreateInstance();
                _bulletSoundInstance[i].Volume = .7f;
            }
        }
        //load images and sounds
        protected override void LoadContent()
        {
            base.LoadContent();
            _playerIcon = Game.Content.Load<Texture2D>("Images/Player/hairmale");
            _playerModel = Game.Content.Load<Texture2D>("Images/Player/male_walkcycle");
            _attackTexture = Game.Content.Load<Texture2D>("Images/Player/bullet");
            _bulletSound = Game.Content.Load<SoundEffect>("Sound/Player/Magic");

            _playerPainSound = Game.Content.Load<SoundEffect>("Sound/Player/pain");
            _playerDeathSound = Game.Content.Load<SoundEffect>("Sound/Player/death");
            _alienDeathSound = Game.Content.Load<SoundEffect>("Sound/Enemys/death");
            _deathTexture = Game.Content.Load<Texture2D>("Images/Player/male_hurt");

            _statsFont = Game.Content.Load<SpriteFont>("Fonts/stats");


            _batch = new SpriteBatch(Game.GraphicsDevice);
        }

        public override void Update(GameTime gameTime)
        {
            if (_player.Active)
            {
                var ks = Keyboard.GetState();
                //if player is is wanting to go left or right we need to change postion of player
                if (ks.IsKeyDown(Keys.Right) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X > 0)
                {
                    _player.Position.X += 5;
                    CurrentState = State.Right;
                    //test = new Rectangle(17, 208, 30, 47);
                }
                else if (ks.IsKeyDown(Keys.Left) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X < 0) {
                    _player.Position.X -= 5;
                    CurrentState = State.left;
                }
                //for palyer to go up or down
                else if (ks.IsKeyDown(Keys.Up) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0) {
                _player.Position.Y -= 5;
                    CurrentState = State.Up;
                }
            else if (ks.IsKeyDown(Keys.Down) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < 0)
                {
                _player.Position.Y += 5;
                    CurrentState = State.Down;
                }

                //if player is trying to go out of bounds
                if (_player.Position.X < _player.ActualWidth / 2 + 70) _player.Position.X = _player.ActualWidth / 2 + 70;

                else if (_player.Position.X > Game.Window.ClientBounds.Width - _player.ActualWidth / 2 - 70)
                    _player.Position.X = Game.Window.ClientBounds.Width - _player.ActualWidth / 2 -70;


                //allow player to shoot
                if ((_totalForBulletElapsed += gameTime.ElapsedGameTime) > _minForBullet && ks.IsKeyDown(Keys.Space) || (_totalForBulletElapsed += gameTime.ElapsedGameTime) > _minForBullet && GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.A))
                {
                    _totalForBulletElapsed = TimeSpan.Zero;
                    CreateBullet();
                }
            }

            for (int i = 0; i < _bullets.Length; i++)
            {
                var bullet = _bullets[i];
                if (bullet.Active)
                {
                    bullet.Update(gameTime);
                    if (bullet.Position.Y < -15 || bullet.Position.Y >  900 || bullet.Position.X < -15 || bullet.Position.X > 1300)
                    {
                        // out of bounds
                        bullet.Active = false;
                    }
                    else
                    {
                        foreach (var alien in ForestGame.Current.AliensComponent.Aliens)
                            if (alien != null && alien.Active && !alien.IsDying && bullet.Collide(alien))
                            {
                                // alien hit!
                                bullet.Active = false;
                                alien.Hit(Power);
                                Score += alien.Type.Score * ForestGame.Current.Level;
                                //play a death sound
                                _explodeSoundInstance[i].Stop();
                                _explodeSoundInstance[i].Play();
                                break;
                            }
                    }
                }
            }

            _player.Update(gameTime);
            _playerDeath.Update(gameTime);

            base.Update(gameTime);
        }

        private void CreateBullet()
        {
            while (_bullets[_lastBulletIndex].Active) {
                _lastBulletIndex = (_lastBulletIndex + 1) % _bullets.Length;
               }
            Sprite bullet = _bullets[_lastBulletIndex];
            bullet.Position = _player.Position;
            bullet.Active = true;

            bullet.Velocity.Y = 0;
            bullet.Velocity.X = 0;

            //allow user to shoot in diffrent dirrections
            if (CurrentState == State.Up) {
            bullet.Velocity.Y = -9;
            }
            else if (CurrentState == State.Down)
            {
                bullet.Velocity.Y = 9;
            }
            else if (CurrentState == State.left)
            {
                bullet.Velocity.X = -9;
            }
            else if (CurrentState == State.Right)
            {
                 bullet.Velocity.X= 9;
            }
            else
            {
                //bullet.Velocity.Y = +9;
            }

            _bulletSoundInstance[_lastBulletIndex].Play();
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            _player.Draw(gameTime, _batch);


            foreach (var bullet in _bullets)
                if (bullet.Active)
                    bullet.Draw(gameTime, _batch);

            _playerDeath.Draw(gameTime, _batch);
            //give the user a hud
            //allow player to see their score
            //give player a goal (beat highscore)
            //show player what level they are on
            _batch.DrawString(_statsFont, string.Format("Score: {0,5}", Score), new Vector2(30, 30), Color.Yellow,0, Vector2.Zero, 1, 0, 0);
            _batch.DrawString(_statsFont, string.Format("Highscore: {0,5}", ForestGame.Current.IntroComponent.Highscore), new Vector2(30, 50), Color.Yellow, 0, Vector2.Zero,1, 0, 0);
            _batch.DrawString(_statsFont, string.Format("Level: {0}", ForestGame.Current.Level), new Vector2(30, 70), Color.Yellow, 0, Vector2.Zero, 1,0,0);

            //draw lives
            //in this case player heads
            for (int i = 0; i < Lives; i++) {
                _batch.Draw(_playerIcon, new Vector2(i * 50 + 30, 90), new Rectangle(8, 250, 24, 22), Color.White, 0, Vector2.Zero, 2, 0, 0);
            }

            _batch.End();
            base.Draw(gameTime);


        }

        public void PlayerHit(int power = 1)
        {
            // setup explosion
            //_playerExplodeSound.Play();
            if (Lives == 0 || Lives < 0)
            {
                _player.Active = false;
                _playerDeath.Position = _player.Position;
                _playerDeathSound.Play();
                ForestGame.Current.GotoState(GameState.GameOver);
                _playerDeath.Active = true;
            }else {
            Lives = Lives - 1;
                //indicate player hit
                _playerPainSound.Play();

                _player.Active = true;
            }
        }

        //set player to have no score
        //make player active
        //give player three lives
        public void InitPlayer()
        {
            Score = 0; Lives = 3;
            _player.Active = true;
        }
    }
}
