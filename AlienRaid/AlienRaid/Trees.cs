using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Assignment1;

namespace AlienRaid
{
    public class StarfieldComponent : DrawableGameComponent
    {
        Sprite[] _stars = new Sprite[50];
        Random _rnd = new Random();
        Texture2D _starTexture;
        SpriteBatch _batch;

        public StarfieldComponent(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        public override void Initialize()
        {
            base.Initialize();

            for (int i = 0; i < _stars.Length; i++)
            {
                Sprite star = new Sprite(_starTexture);
                star.Color = new Color(_rnd.Next(256), _rnd.Next(256), _rnd.Next(256), 128);
                star.Position = new Vector2(_rnd.Next(Game.Window.ClientBounds.Width), _rnd.Next(Game.Window.ClientBounds.Height));
                star.Velocity.Y = (float)_rnd.NextDouble() * 5 + 2;
                _stars[i] = star;
            }
        }

        protected override void LoadContent()
        {
            _starTexture = Game.Content.Load<Texture2D>("Images/star");
            _batch = new SpriteBatch(Game.GraphicsDevice);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            int height = Game.Window.ClientBounds.Height;

            for (int i = 0; i < _stars.Length; i++)
            {
                var star = _stars[i];
                if ((star.Position.Y += star.Velocity.Y) > height)
                {
                    // "generate" a new star
                    star.Position = new Vector2(_rnd.Next(Game.Window.ClientBounds.Width), -_rnd.Next(20));
                    star.Velocity.Y = (float)_rnd.NextDouble() * 5 + 2;
                    star.Color = new Color(_rnd.Next(256), _rnd.Next(256), _rnd.Next(256), 128);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            foreach (var star in _stars)
                star.Draw(gameTime, _batch);
            _batch.End();
            base.Draw(gameTime);
        }
    }
}
