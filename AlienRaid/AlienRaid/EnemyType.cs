﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Assignment1 {
	class EnemyType {
		public TimeSpan AnimationRate = TimeSpan.FromMilliseconds(500);
		public string Name;
		public int Score;
		public int Space;
        public int Hitpoint;
		public float MaxXSpeed;
		public float MaxYSpeed;
		public Texture2D Texture;
		public int Frames = 1;
		public bool IsHorizontal = true;
		public Rectangle? FirstFrame;
	}
}
