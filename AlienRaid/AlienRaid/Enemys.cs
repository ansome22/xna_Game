﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Assignment1 {
	class Enemys : Sprite {
		public readonly bool IsBoss;
		public readonly EnemyType Type;

		public int HitPoint;

        private readonly Random _rnd = new Random();

        //define enemy
		public Enemys(EnemyType type, bool isBoss)
			: base(type.Texture, type.FirstFrame, type.Frames, type.IsHorizontal, type.Space) {
            IsBoss = isBoss;
            Type = type;
            Origin.X = type.FirstFrame.Value.Width / 2;
            AnimationInterval = type.AnimationRate;
            HitPoint = isBoss ? _rnd.Next(10) + 3 * ForestGame.Current.Level : ForestGame.Current.Level;
        }

		public override void Update(GameTime gameTime) {
            if (Active && !_isDying)
            {
                if (Position.X < 100 || Position.X > ForestGame.Current.Window.ClientBounds.Width -100
                    // randomly change direction
                    || _rnd.Next(100) < ForestGame.Current.GetCurrentLevelData().ChangeDirChance)
                    Velocity.X = -Velocity.X;
                if (Position.Y > ForestGame.Current.Window.ClientBounds.Height + 40)
                {
                    Active = false;
                    OutOfBounds?.Invoke(this);
                }
                //make bosses moves eratice
                if (IsBoss && Position.Y > 100)
                {
                    Velocity.Y = 0;
                    Position.Y = 100;
                }
            }
            else if (_isDying)
            {
                if (Color.R > 4)
                    Color.R -= 4;
                else
                {
                    Active = false;
                }
                return;
            }

            base.Update(gameTime);
        }

		public event Action<Enemys> OutOfBounds;
		public event Action<Enemys> Killed;

		private bool _isDying;

		public bool IsDying { get { return _isDying; } }

		public virtual void Hit(int power)    {
			if((HitPoint -= power) <= 0) {
                //Spawn Powerup
                //Spawn(Position.X, Position.Y);
				// alien killed, start death sequence
				_isDying = true;
				Color = new Color(255, 0, 0);
				if(Killed != null)
					Killed(this);

			}
		}
	}
}
