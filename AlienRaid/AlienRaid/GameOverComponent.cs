using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Assignment1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameOverComponent : DrawableGameComponent
    {
        SpriteBatch _batch;
        SpriteFont _font, _smallFont;
        KeyboardState _previousKS;

        public int level = 1;

        public GameOverComponent(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        protected override void LoadContent()
        {
            _font = Game.Content.Load<SpriteFont>("fonts/bigfont");
            _smallFont = Game.Content.Load<SpriteFont>("fonts/Stats");

            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();

            _batch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            var ks = Keyboard.GetState();
            if (_previousKS.IsKeyDown(Keys.Enter) && ks.IsKeyUp(Keys.Enter) || GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.A))
            {
                ForestGame.Current.StartNewGame(level);
                ForestGame.Current.GotoState(GameState.InPlay);
            }
            _previousKS = ks;

            //if player has higher score than highscore
            //there is now a new highscore
            if (ForestGame.Current.IntroComponent.Highscore < ForestGame.Current.PlayerComponent.Score)
            {
                ForestGame.Current.IntroComponent.Highscore = ForestGame.Current.PlayerComponent.Score;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //draw background
            GraphicsDevice.Clear(Color.Red);
            //tell user they have died
            //give them end of results
            int Score = ForestGame.Current.PlayerComponent.Score;
            _batch.Begin();
            _batch.DrawString(_font, "YOU DIED", new Vector2(400, 350), Color.DarkRed);
            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                _batch.DrawString(_smallFont, "Press A to play again or BACK to quit game", new Vector2(350, 450), Color.Cyan);
            }else
            {
                _batch.DrawString(_smallFont, "Press ENTER to play again or ESC to quit game", new Vector2(350, 450), Color.Cyan);
            }

            _batch.DrawString(_font, "Your Score: " + Score, new Vector2(350, 550), Color.AliceBlue);

            _batch.DrawString(_font, "HighScore: "+ForestGame.Current.IntroComponent.Highscore, new Vector2(350, 650), Color.AliceBlue);

            _batch.End();

            base.Draw(gameTime);
        }
    }
}
