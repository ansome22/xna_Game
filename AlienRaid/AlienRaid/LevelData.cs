﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assignment1 {
	class AlienSelectionData {
		public EnemyType Alien;
		public int Chance;
	}

	class LevelData {
		public int Number;
		public TimeSpan AlienGenerationTime;
		public int MaxActiveAliens;
		public int TotalAliensToFinish;
		public EnemyType Boss;
		public List<AlienSelectionData> SelectionData;
		public int ChangeDirChance;
		public int FireChance;
		public int MaxAlienBullets;
	}
}
