using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Assignment1 {
	/// <summary>
	/// This is a game component that implements IUpdateable.
	/// </summary>
	public class EnemysComponent : DrawableGameComponent {
		SpriteBatch _batch;
		Enemys[] _enemys = new Enemys[24];

        // alien bullets
        Sprite[] _bullets = new Sprite[24];
        SoundEffectInstance[] _bulletSoundInst = new SoundEffectInstance[24];
        SoundEffect _bulletSound;
        Texture2D _texture;
        int _totalBullets, _currentBullet;

        int _currentAlienIndex;
		Random _rnd = new Random();
		TimeSpan _elapsedTime;
		int _currentLiveAliens, _totalKilledAliens;


        private TimeSpan _delayCount, _delay;

        public TimeSpan Delay
        {
            get { return _delay; }
            set
            {
                _delay = value;
                _delayCount = TimeSpan.Zero;
            }
        }


        internal IEnumerable<Enemys> Aliens { get { return _enemys; } }

		public EnemysComponent(Game game)
			: base(game) {
			// TODO: Construct any child components here
		}

		/// <summary>
		/// Allows the game component to perform any initialization it needs to before starting
		/// to run.  This is where it can query for any required services and load content.
		/// </summary>
		public override void Initialize() {
			base.Initialize();

			_batch = new SpriteBatch(Game.GraphicsDevice);
		}

        protected override void LoadContent()
        {
            _bulletSound = Game.Content.Load<SoundEffect>("Sound/Player/Magic");
            _texture = Game.Content.Load<Texture2D>("Images/Enemys/shot");
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime) {
			// get current level data
			LevelData data = ForestGame.Current.GetCurrentLevelData();

            foreach (var bullet in _bullets)
            {
                if (bullet != null && bullet.Active)
                {
                    bullet.Update(gameTime);
                    if (bullet.Position.Y > Game.Window.ClientBounds.Height + 30)
                    {
                        bullet.Active = false;
                        _totalBullets--;
                    }
                    else if (bullet.Collide(ForestGame.Current.PlayerComponent.Player))
                    {
                        ForestGame.Current.PlayerComponent.PlayerHit();
                        bullet.Active = false;
                        _totalBullets--;
                    }
                }
            }

            if (Delay > TimeSpan.Zero)
            {
                if ((_delayCount += gameTime.ElapsedGameTime) >= Delay)
                    Delay = TimeSpan.Zero;
            }

            if (_delay == TimeSpan.Zero && (_elapsedTime += gameTime.ElapsedGameTime) > data.AlienGenerationTime)
            {
                _elapsedTime = TimeSpan.Zero;
                var alien = CreateEnemy();
                if (alien != null)
                {
                    _currentLiveAliens++;
                    while (_enemys[_currentAlienIndex] != null && _enemys[_currentAlienIndex].Active)
                        _currentAlienIndex = (_currentAlienIndex + 1) % _enemys.Length;
                    _enemys[_currentAlienIndex] = alien;
                }
            }

            var pc = ForestGame.Current.PlayerComponent;

			foreach(var enemy in _enemys)
                if (enemy != null && enemy.Active)
                {
                    enemy.Update(gameTime);
                    if (!enemy.IsDying && pc.Player.Active && enemy.Collide(pc.Player))
                    {
                        // player hit!
                        enemy.Hit(3);   // enemy hit, too!
                        pc.PlayerHit();
                    }
                    if (_delay == TimeSpan.Zero && _totalBullets < data.MaxAlienBullets && _rnd.Next(100) < data.FireChance)
                        CreateBullet(enemy);

                }

            base.Update(gameTime);
		}

		private Enemys CreateEnemy() {
            var data = ForestGame.Current.GetCurrentLevelData();
            if (_currentLiveAliens > data.MaxActiveAliens) return null;

            if (_totalKilledAliens == data.TotalAliensToFinish)
            {
                _totalKilledAliens++;
                var boss = CreateBoss();
                boss.Killed += delegate {
                    _currentLiveAliens--;
                    if(ForestGame.Current.Level != 4)
                    {
                        ForestGame.Current.Level = ForestGame.Current.Level + 1;
                        ForestGame.Current.InitLevel(ForestGame.Current.Level);
                    }

                    _totalKilledAliens = 0;
                };
                return boss;
            }

            int chance = 0;
            int value = _rnd.Next(100);
            foreach (var sd in data.SelectionData)
            {
                if (value < sd.Chance + chance)
                {
                    // selected
                    Enemys alien = new Enemys(sd.Alien, false);
                    alien.Position = new Vector2((float)(_rnd.NextDouble() * Game.Window.ClientBounds.Width/1.4 + 90), -20);
                    alien.Velocity = new Vector2((float)(_rnd.NextDouble() * alien.Type.MaxXSpeed * 2 - alien.Type.MaxXSpeed), (float)(_rnd.NextDouble() * alien.Type.MaxYSpeed + 1));
                    alien.Scale = 2;
                    alien.ZLayer = .7f;
                    alien.OutOfBounds += a => {
                        _currentLiveAliens--;
                    };
                    alien.Killed += a => {
                        _currentLiveAliens--;
                        _totalKilledAliens++;
                    };
                    return alien;
                }
                chance += sd.Chance;
            }
            return null;
        }

        private Enemys CreateBoss()
        {
            var data = ForestGame.Current.GetCurrentLevelData();
            Enemys alien = new Enemys(data.Boss, true);
            alien.Position = new Vector2((float)(_rnd.NextDouble() * Game.Window.ClientBounds.Width), -90);
            alien.Velocity = new Vector2((float)(_rnd.NextDouble() * alien.Type.MaxXSpeed * 2 - alien.Type.MaxXSpeed), 5.0f);
            alien.Scale = 2;
            return alien;
        }


        public override void Draw(GameTime gameTime) {
			_batch.Begin();
			foreach(var alien in _enemys)
				if(alien != null && alien.Active)
					alien.Draw(gameTime, _batch);

            foreach (var bullet in _bullets)
                if (bullet != null)
                    bullet.Draw(gameTime, _batch);

            _batch.End();
			base.Draw(gameTime);
		}

		public void InitAliens() {
			for(int i = 0; i < _enemys.Length; i++)
				_enemys[i] = null;
			_totalKilledAliens = _currentLiveAliens = _currentAlienIndex = 0;
		}


        private void CreateBullet(Enemys alien)
        {
            while (_bullets[_currentBullet] != null && _bullets[_currentBullet].Active)
                _currentBullet = (_currentBullet + 1) % _bullets.Length;
            Sprite bullet = _bullets[_currentBullet];
            if (bullet == null)
            {
                bullet = new Sprite(_texture, new Rectangle(1,3, 13, 14),3,true,3);
                bullet.Scale = 2;
                bullet.ZLayer = .5f;
                _bullets[_currentBullet] = bullet;
                _bulletSoundInst[_currentBullet] = _bulletSound.CreateInstance();
                _bulletSoundInst[_currentBullet].Volume = .7f;
            }
            bullet.Position = alien.Position + new Vector2(0, alien.ActualHeight);
            bullet.Velocity.Y = alien.Velocity.Y > 0 ? 1.8f * alien.Velocity.Y : _rnd.Next(10) + 3;
            bullet.Active = true;
            _bulletSoundInst[_currentBullet].Play();
            _totalBullets++;
        }


    }
}
